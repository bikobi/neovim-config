# Neovim configuration

My simple configuration of [Neovim](https://neovim.io/), in Lua (thanks to [Gregory Witek's article](https://www.notonlycode.org/neovim-lua-config/)).  
The configuration includes:

- basic general settings;
- some mappings (i.e. autoinsert matching pairs of characters);
- [Packer](https://github.com/wbthomason/packer.nvim#quickstart) as plugin manager (managing itself);
- LSP configuration for Go;
- A few plugins for Go autocomplete.

Note that you need to have `gopls` installed for the LSP and autocomplete to work.  
Run `:PackerInstall` to install the plugins.

> **Please keep in mind that (Neo)vim's configuration is very personal**, so it is unlikely that mine will fit your needs without further adjustment. But that is precisely what makes (Neo)vim so powerful.

I inserted comments and links to resources that I used to make the configuration more clear.

