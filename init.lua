-- https://github.com/arnvald/viml-to-lua/
-- https://www.notonlycode.org/neovim-lua-config/
require('settings')
require('netrw-config')
require('mappings')
require('plugins')
require('lsp')
