-- https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
vim.g.netrw_banner = 0 -- 0/1 toggle the banner; you can always press I to temporarily show it.
vim.g.netrw_liststyle = 3 -- tree-style view
vim.g.netrw_browse_split = 3 -- open new files in: 1 new horizontal split; 2 new vertical split; 3 new tab; 4 previous window
vim.g.netrw_altv = 1 -- left splitting
vim.g.netrw_winsize = 20 -- netrw split will take up 25% of the window
