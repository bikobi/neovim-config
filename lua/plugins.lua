 -- https://github.com/wbthomason/packer.nvim

vim.cmd [[packadd packer.nvim]]


return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- nvim-lspconfig: https://github.com/neovim/nvim-lspconfig
  -- see also: https://www.youtube.com/watch?v=puWgHa7k3SY
  use 'neovim/nvim-lspconfig'

  -- lsp autocomplete
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'

  -- not sure why i left these here
  use 'L3MON4D3/LuaSnip'
  -- use 'saadparwaiz1/cmp_luasnip'

  -- vim-go: https://github.com/fatih/vim-go/
  -- use {
  --   'fatih/vim-go',
  --   ft = {'go'}, -- load the plugin only in .go files
  -- }
  
  -- Gruvbox theme
  use "ellisonleao/gruvbox.nvim"

  -- Markdown markdown-preview
  -- install without yarn or npm
  use({
      "iamcco/markdown-preview.nvim",
      run = function() vim.fn["mkdp#util#install"]() end,
  })
  
end)
