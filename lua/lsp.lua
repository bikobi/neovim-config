-- nvim-cmp configuration (for completion, maybe move to another file?)
local capabilities = require('cmp_nvim_lsp').default_capabilities() -- keep this line at the top, for nvim-cmp

vim.opt.completeopt = {"menu", "menuone", "noselect"}

local cmp = require'cmp'

cmp.setup({
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
    end,
  },
  window = {
    -- completion = cmp.config.window.bordered(),
    -- documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' }, -- For luasnip users.
  }, {
    { name = 'buffer' },
  })
})

-- gopls
require'lspconfig'.gopls.setup{
    capabilities = capabilities,
    on_attach = function()
        vim.keymap.set("n", "K", vim.lsp.buf.hover, {buffer=0}) -- show hover documentation
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer=0}) -- go to definition
        vim.keymap.set("n", "gT", vim.lsp.buf.type_definition, {buffer=0}) -- go to type definition
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, {buffer=0}) -- go to implementation
        vim.keymap.set("n", "<leader>dn", vim.diagnostic.goto_next, {buffer=0}) -- diagnostic next: jump to next error
        vim.keymap.set("n", "<leader>dp", vim.diagnostic.goto_prev, {buffer=0}) -- diagnostic previous: jump to previous error
        vim.keymap.set("n", "<leader>r", vim.lsp.buf.rename, {buffer=0}) -- rename variable through whole document, smartly
        vim.keymap.set("n", "<leader>A", vim.lsp.buf.code_action, {buffer=0}) -- show all available code actions
    end,
}
