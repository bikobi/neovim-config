function map(mode, keys, mapping)
    vim.api.nvim_set_keymap(mode, keys, mapping, { noremap = true, silent = true })
end

-- Autoinsert matching pairs in insert mode
map("i", "(", "()<Esc>i")
map("i", "[", "[]<Esc>i")
map("i", "{", "{}<Esc>i")
map("i", "\"", "\"\"<Esc>i")
-- map("i", "'", "''<Esc>i")
map("i", "`", "``<Esc>i")

-- Toggle nerdtree
map("n", "<leader>e", "<cmd>Lex<cr>")

-- Toggle terminal window
map("n", "<leader>t", "<cmd>below 10sp term://$SHELL<cr>i")

-- Esc to go to normal mode in terminal
map("t", "<C-s>", "<C-\\><C-n>")
map("t", "<Esc><Esc>", "<C-\\><C-n>")

