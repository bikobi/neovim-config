HOME = os.getenv("HOME")

vim.g.mapleader = ' '

-- Basic settings
vim.opt.encoding = "utf-8"

-- Display
vim.opt.showmatch = true -- show matching brackets
vim.opt.scrolloff = 8 --always show 8 lines from the edge of the screen

vim.opt.foldmethod = "indent" -- use indenation to create folds (see :h foldmethod); use za to create folds
-- vim.opt.foldenable = false  -- do not start with folds closed by default when opening a file
vim.opt.foldlevelstart = 99 -- start editing with all folds open
vim.opt.wrap = false
vim.opt.showbreak = "↪"

-- Sidebar
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.signcolumn = "no"
vim.opt.showcmd = true -- set to false to hide the keypresses in bottom-right

-- Search
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = false

-- White characters
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.tabstop = 4 -- 1 tab = 4 spaces
vim.opt.shiftwidth = 4
vim.opt.formatoptions = "qnj1" -- q  - comment formatting; n - numbered lists; j - remove comment when joining lines; 1 - don't break after one-letter word
vim.opt.expandtab = true --expand tabs to spaces

-- Commands mode
vim.opt.wildmenu = true

vim.opt.background = "dark"
vim.cmd("colorscheme gruvbox")

-- Go options
vim.cmd [[autocmd BufWritePre <buffer> lua vim.lsp.buf.format()]] -- autoformat on save; see https://www.jvt.me/posts/2022/03/01/neovim-format-on-save/
-- TODO: autoimports on save
